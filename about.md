---
layout: page
title: About
permalink: /about/
---


<div  class="text-block">
    <h2>AboutOP-TEE</h2>
    <p>OP-TEE is an open source project which containsa full implementation to make up a complete Trusted Execution Environment. The project has roots in a proprietary solution, initially created by ST-Ericsson and then owned and maintained by STMicroelectronics. In2014, Linaro started workingwith STMicroelectronics to transform the proprietary TEE solution intoan open source TEE solution instead.</p>
    <p>In September 2015, the ownership was transferred to Linaro. Today it isone of the key security projects in Linaro, with several of Linaro’s members supporting and using it.</p>
    
    <h2>Mailing lists:</h2>
    <p>op-tee@linaro.org (reach OP-TEE developers)<br>
    <a href="https://lists.linaro.org/mailman/admindb/tee-dev" target="_blank">tee-dev</a> (public discussion about TEE in general)</p>
    
    <p>Issues, bugs etc?<br>
    Please file an issue onour “<a href="https://github.com/OP-TEE/optee_os/issues">Issues</a>” list at GitHub.</p>

</div>
<div class="text-block center">
      <hr>
      <p><strong>Supporting companies</strong></p>
</div>
<div  class="text-block center">
    <p>
        <img src="https://www.linaro.org/wp-content/uploads/2016/11/linaro.jpg" width="165" height="99" class="">
        <img class="lightbox-false" title="Wind" src="https://www.linaro.org/wp-content/uploads/2016/07/wind-1.jpg" alt="Wind" width="165" height="99">
        <img class="lightbox-false" title="applus laboratories" src="https://www.op-tee.org/wp-content/uploads/2014/09/applus-laboratories.jpg" alt="applus laboratories" width="165" height="99">
      </p>
</div>
<div class="spacer" data-height="40px"></div>